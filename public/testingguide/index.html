<!DOCTYPE html>
<html>
    <head lang="en">
        <title>Testing Guide</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="styles.css">
    </head>
    <body>
        <div id="content">
            <h1>Haaukins challenge testing guide</h1>
            <p>This guide is intended to take you through the challenge testing process, from beginning to approval.</p>

            <h2>Before testing</h2>
            <p>As a tester, you are the last resort in the lifecycle of a challenge. Once you approve it, there is a good chance no one will check the challenge again for mistakes, therefore you must take great care during testing.</p>
            <p>You can start testing the challenge, when the development deadline is over <strong>AND</strong> the developer marked the challenge as <code>I assure it is fully ready to a competition</code>. If according to the spreadsheet the challenge is not ready for testing but the deadline is over, contact the developer on Slack and ask about the situation. If you can not reach the developer, contact <a href="mailto:mhto@es.aau.dk">Mateus</a> about the issue (preferably on Slack)!</p>
            <p>When you start testing the challenge, chect the spreadsheet and get familiar with the challenge. Check if:</p>
            <ul>
                <li>challenge name is filled out</li>
                <li>GitLab link is filled out (if there is no link to the challenge's repository and you can not obviously find the challenge on GitLab by its name, contact the developer immediately!)</li>
                <li>short description of challenge is filled out</li>
            </ul>
            <p>See what is the category and difficulty of the challenge, as a tester it is also your job to validate if the developed challenge matches with the given category and difficulty.</p>
            <p>Now you can set the <code>Tester status</code> column to <code>Testing</code>.</p>

            <h2>Manual inspection</h2>
            <p>In the repository have a look around. There should be no other branches than <code>main</code> and there should be no open merge requests! If this is not the case, reach out to the developer and ask if they are truly finished with the challenge and to close all requests either by merging or cancelling, and to close other branches as well!</p>
            <p>When a branch is merged back, someone is checking if the <code>challenge.yml</code> and <code>.gitlab-ci.yml</code> files are written correctly. However, it can happen that something slips through the system, and there is a misconfiguration in any of these files. When you finally testing the challenge on Haaukins and the flag is not working for some reason, it might be a misconfiguration in one of them.</p>
            <p>Determine if it is a dynamic or static challenge based on the <code>challenge.yml</code> file, and check if it set to secret! Every new challenge must be secret!</p>
            <p>Look around in the <code>src</code> and <code>solution</code> folder. See the contents of the challenge, see what it does, what files it has. Spend time on getting familiar with the challenge accordingly to the difficulty of the challenge. If applicable, a solution script should also be provided, check this one as well. Also, check if solution steps are included both in <code>readme.md</code> and in <code>challenge.yml</code> files and are detailed enough, that following them the challenge is easily solvable!</p>
            <p>During testing our main goal is not to ensure better code quality, but to validate that the challenge is working and deployable in real environment.</p>
            <p>Check if the flag format is appropriate! It should be <code>DDC{...}</code> or <code>HKN{...}</code> and alpahanumeric characters, <code>-</code> or <code>_</code>, minimum 6 - maximum 50 long, nothing more!</p>

            <h2 id="static">Static challenge testing</h2>
            <p>For a static challenge, there should be no Docker files! Check if you have the NextCloud download link in the <code>challenge.yml</code> file's description part and in the <code>readme.md</code> as well.</p>
            <p>In the download link, the domain instead of <code>ntp-event.dk:8443</code> should be <code>haaukins.com</code>!</p>
            <p>Check if the download link works, downloads the correct material and if it is a direct download link, which means if you click on it, the content starts downloading immediately. Also check, that the downloadable content is a zip file and its filename follows the naming convention from the challengeguide.</p>
            <p>Download the challenge contents, and solve the challenge according to the provided solution. If applicable, play around with the challenge, torture it, see if it is not possible to solve in unintended ways!</p>
            <p>If you found the correct flag with the solution, you are good to go!</p>
            <p>In the case of reverse engineering or binary exploitation challenges, see if the participant has challenge content for both arm64 and x64 machines, unless the challenge is specifically about a certain architecture!</p>

            <h2>Dynamic challenge testing</h2>
            <p>For a dynamic challenge there should be at least one Docker file, but can be multiple files as well. Check these, pay attention that the challenge uses a versioned Docker image, so that the challenge will not break because of a breaking image update on the Docker Hub.</p>
            <p>There should be a short description, or either a <code>docker-compose.yml</code> file that says how to run the challenge locally. Test it on your local machine, if you see it fit and have the time for it, but it is more important to test the challenge on a real Haaukins environment!</p>
            <p>It is possible, that a challenge uses both Docker for server and NextCloud for downloadable content! Make sure that the NextCloud link works accordingly as in the <a href="#static">Static challenge</a> testing description.</p>
            <p>With dynamic challenges pay special attention that all servers should work in Haaukins!</p>

            <h2>DDC specific checks</h2>
            <p>With DDC challenges there are some special requirements. You must pay greater attention to the previously discussed aspects, but other than that:</p>
            <ul>
                <li>The flag for <strong>must be</strong> <code>DDC{...}</code></li>
                <li>The description which is given to the participant <strong>must be</strong> in Danish! Other challenge content can be in English.</li>
                <li>The challenge <strong>must not</strong> contain inappropriate content for someone under 18 years old!</li>
            </ul>

            <h2>Final check and approval</h2>
            <p>We usually develop challenges for a certain event, which means a group of challenges are ready for testing at the same time! Therefore, we set up a testing event on real Haaukins environment and announce it on Slack. These testing events will be ready by the time the testing is finished with the already finished challenges! Later finished challenges will be added accordingly! We might restart the event, so you might need to sign up multiple times!</p>
            <p>If there is no specific testing event, please ask <a href="mailto:mreite23@student.aau.dk">Márton</a> (preferably on Slack) to setup an event for you where you can test the challenge!</p>
            <p>Sign up for the testing event and test the challenge again! Most importantly, check if you can submit the flag you found!</p>
            <p>If everything is good, head back to the spreadsheet and set the <code>Tester status</code> column to <code>Approved</code>.</p>
            <p>If you tested the challenge and you found one or more mistakes, send a direct message to the developer where you state the issues, and ask them to fix it! In the meantime, head to the spreadsheet and set the <code>Tester status</code> column to <code>Needs repair</code>.</p>
            <p>Testing and repair must be done by the testing deadline, so make sure the developer has enough time to and does fix the requested issues!</p>
        </div>
        <footer>Version: 1.0. All rights reserved. Haaukins, 2024.</footer>
    </body>
</html>
