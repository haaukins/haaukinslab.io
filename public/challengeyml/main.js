import { downloadZip } from "./client-zip.min.js";

// Counters for different records to assign different IDs to each record
// Never decreases (to avoid assigning same ID to different records)! Only resets!
var imageID = 0;
var dnsID = 0;
var envID = 0;
var flagID = 0;

$(document).ready(function () {
    // Quick and dirty export
    window.update = update;
    // TODO Revert back after DDC
    //window.updateFlagTypePlaceholder = updateFlagTypePlaceholder;
    window.onCheckStatic = onCheckStatic;
    window.downloadProject = downloadProject;

    onCheckStatic();
    update();
});

var trimSlashes = function (s) {
    return s.trim().replace(/^\/+|\/+$/g, '');
}

// TODO Revert back after DDC
/*var updateFlagTypePlaceholder = function (id) {
    var placeholder = $(`#flagtype${id}`).is(":checked") ? "Environment variable" : "HKN{...} / DDC{...}";
    $(`#flag${id}`).attr("placeholder", placeholder);
}*/

function generateTag(name) {
    return name.trim()
        .toLowerCase()
        .split('')
        .filter(c => c >= 'a' && c <= 'z' || c >= '0' && c <= '9')
        .join('');
}

function validateNextCloudLink(link) {
    link = link.replace('ntp-event.dk:8443', 'haaukins.com');
    if (!link.includes('https://nextcloud.haaukins.com')) {
        return '!!!! ERROR IN NEXTCLOUD LINK: wrong domain! Change ntp-event.dk:8443 to haaukins.com !!!!';
    }

    if (!link.endsWith('.zip')) {
        return '!!!! ERROR IN NEXTCLOUD LINK: downloadable content is not a zip file !!!!';
    }

    if (!link.match('^https:\/\/nextcloud\.haaukins\.com\/s\/[a-zA-Z0-9]+\/download\/[a-zA-Z0-9_-]+\.zip$')) {
        return '!!!! ERROR IN NEXTCLOUD LINK: wrong format, probably not direct download link !!!!';
    }
    return link;
}

function validateFlagFormat(flag){
    return flag.match('(HKN|DDC)\{[a-zA-Z0-9-_]{6,50}\}') ? flag : 'WRONG FLAG FORMAT! FLAG MUST MATCH: (HKN|DDC)\{[a-zA-Z0-9-_]{6,50}\}!!!!';
}

var getChallenges = function (parent = "#images") {
    return $(parent).find(".challenge").get().map(c => ({
        title: $(c).find("input.flagname").val(),
        type: $(c).find("input[type='checkbox']").is(":checked") ? "env" : "static",
        flag: $(c).find("input.flag").val(),
        points: $(c).find("input.flagpoints").val(),
        category: $(c).find("select.flagcategory").val(),
        difficulty: $(c).find("select.flagdifficulty").val(),
        link: $(c).find("input.flaglink").val(),
        description: $(c).find("textarea.flagdesc").val(),
        solution: $(c).find("textarea.flagsolution").val(),
    }));
}

var getImages = function () {
    return $(".image").get().map(i => ({
        folder: trimSlashes($(i).find("input.imagefolder").val()),
        dns: $(i).find(".dnsrecord").get().map(d => ({
            type: $(d).find("select.dnstag").val(),
            value: $(d).find("input.dnsvalue").val(),
        })),
        env: $(i).find(".envrecord").get().map(e => ({
            key: $(e).find("input.envname").val(),
            value: $(e).find("input.envvalue").val(),
        })),
        challenges: getChallenges(i),
    }));
}

var onCheckStatic = function () {
    $("#images").html("");
    imageID = 0;
    flagID = 0;
    dnsID = 0;
    envID = 0;

    var isStatic = !$("#entry-type").is(":checked");
    if (isStatic) {
        $("#addImage").hide();
        $("#addStaticImage").show();
        $("#imagepathdiv").hide();
        $("#addStaticImage").click();
    } else {
        $("#addImage").show();
        $("#addStaticImage").hide();
        $("#imagepathdiv").show();
        $("#addImage").click();
    }
}

var updateReadme = function () {
    var entryName = $("#challengename").val();
    var entryDescription = $("#od").val();
    var prereqs = $("#prereqs").val();
    var outcome = $("#outcome").val();
    var isStatic = !$("#entry-type").is(":checked");

    var images = getImages();
    var challenges = getChallenges();
    var multi = challenges.length > 1;

    var links = challenges.map(challenge => challenge.link).filter(e => e);
    var domains = images.map(image =>
        image.dns.find(d => d.type === "A")
    ).filter(e => e).map(d => d.value);

    var readme = `# ${entryName}

${entryDescription}

# Flag${multi ? 's' : ''}

${challenges.map(c => c.flag).map(flag => (multi ? '- ' : '') + '\`' + validateFlagFormat(flag) + '\`').join("\n")}\n\n`;

    if (isStatic) {
        readme += `# Nextcloud Link${links.length > 1 ? 's' : ''}\n\n`;
        links.forEach(link => {
            readme += (links.length > 1 ? '- ' : '') + `${validateNextCloudLink(link)}\n`
        });
    } else {
        readme += `# Domain Name${domains.length > 1 ? 's' : ''}\n\n`;
        domains.forEach(domain => readme += (domains.length > 1 ? '- ' : '') + `\`${domain}\`\n`);
    }

    readme += `\n# Description${multi ? 's' : ''}\n\n`;

    if (isStatic) {
        challenges.forEach(challenge => {
            if (multi) {
                readme += `## ${challenge.title}\n\n`;
            }
            readme += `Proposed difficulty: ${challenge.difficulty}\n\n${challenge.description}\n\n`;
            if (challenge.link) {
                challenge.link = validateNextCloudLink(challenge.link);
                readme += `[${trimSlashes(challenge.link).split("/").pop()}](${challenge.link})\n\n`;
            }
        });
    } else {
        images.forEach(image => {
            image.challenges.forEach(challenge => {
                if (multi) {
                    readme += `## ${challenge.title}\n\n`;
                }
                readme += `Proposed difficulty: ${challenge.difficulty}\n\n${challenge.description}\n\n`;
                var domain = image.dns.find(d => d.type === "A");
                if (domain) {
                    readme += `[http://${domain.value}](http://${domain.value})\n\n`;
                }
            });
        });
    }

    readme += `# Prerequisites & Outcome

**Prerequisites**

${prereqs}

**Outcome**

${outcome}

# Solution${multi ? 's' : ''}\n`;

    challenges.forEach(challenge => {
        if (multi) {
            readme += `\n## ${challenge.title}\n`;
        }
        readme += `\n${challenge.solution}\n`;
    });

    $("#output-readme").val(readme);
}

var updateConfigYml = function () {
    var entryName = $("#challengename").val();
    var entryTag = generateTag(entryName);
    var entryCategory = $("#pretag").val();
    var isStatic = !$("#entry-type").is(":checked");
    var entryDescription = $("#od").val();
    var prereqs = $("#prereqs").val();
    var outcome = $("#outcome").val();

    var challenges = getChallenges();
    var multi = challenges.length > 1;

    var challengeYml = `name: ${entryName}
tag: ${entryCategory}${entryTag}
static: ${isStatic}
secret: true
od: |
  ${entryDescription.replaceAll("\n", "\n  ")}
  
  **Prerequisites**
  
  ${prereqs.replaceAll("\n", "\n  ")}
  
  **Outcome**
  
  ${outcome.replaceAll("\n", "\n  ")}
  
  ### Solution${multi ? 's' : ''}\n`;

    // Write all solutions together
    challenges.forEach(challenge => {
        if (multi) {
            challengeYml += `  \n  #### ${challenge.title}\n`;
        }
        challengeYml += `  \n  ${challenge.solution.replaceAll("\n", "\n  ")}\n`;
    });

    challengeYml += "instance:\n";

    if (isStatic) {
        challengeYml += "  - image: dummy\n    flags:\n";
        challenges.forEach((challenge, i) => {
            challengeYml += `      - tag: ${entryTag}-${i + 1}
        name: ${challenge.title}
        static: ${validateFlagFormat(challenge.flag)}
        points: ${challenge.points}
        category: ${challenge.category}
        td: |
          ${challenge.description.replaceAll("\n", "\n          ").trim()}\n`;
            if (challenge.link) {
                challenge.link = validateNextCloudLink(challenge.link);
                challengeYml += `          \n          [${trimSlashes(challenge.link).split("/").pop()}](${challenge.link})\n`;
            }
        })
    } else {
        var challengeIdx = 1;
        var imagebasepath = $("#imagebasepath").val();
        var images = getImages();

        images.forEach((image, i) => {
            var tag = `/${trimSlashes(imagebasepath)}:${entryTag}image${i}`;
            challengeYml += `  - image: registry.gitlab.com${tag}\n`;

            var domain = image.dns.find(d => d.type === "A");

            // Add DNS records
            if (image.dns.length > 0) {
                challengeYml += "    dns:\n";
            }
            image.dns.forEach(dns => {
                challengeYml += `      - name: ${dns.value}\n`;
                challengeYml += `        type: ${dns.type}\n`;
            })

            // Add environment variables
            if (image.env.length > 0) {
                challengeYml += "    env:\n";
            }
            image.env.forEach(env => {
                challengeYml += `      - env: ${env.key}\n`;
                challengeYml += `        value: ${env.value}\n`;
            })

            // Add challenges
            if (image.challenges.length > 0) {
                challengeYml += "    flags:\n";
            }
            image.challenges.forEach(challenge => {
                challengeYml += `      - tag: ${entryTag}-${challengeIdx}
        name: ${challenge.title}\n`
        // TODO: Revert after DDC
        //${challenge.type}: ${challenge.type === 'static' ? validateFlagFormat(challenge.flag) : challenge.flag}
         + `        static: ${validateFlagFormat(challenge.flag)}
        points: ${challenge.points}
        category: ${challenge.category}
        td: |
          ${challenge.description.replaceAll("\n", "\n          ").trim()}\n`;

                if (domain) {
                    challengeYml += `          \n          [http://${domain.value}](http://${domain.value})\n`;
                }
                challengeIdx += 1;
            })
        })
    }

    $("#output-challenge-yml").val(challengeYml);
}

var updateGitlabYml = function () {
    var entryTag = generateTag($("#challengename").val());
    var isStatic = !$("#entry-type").is(":checked");
    var imagebasepath = $("#imagebasepath").val();

    var gitlabYml = "";

    if (!isStatic) {
        gitlabYml += `docker-push-to-gitlab-registery:
  image: docker:latest
  stage: build
  services:
    - docker:dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:`;

        var images = getImages();
        images.forEach((image, i) => {
            var link = `/${trimSlashes(imagebasepath)}:${entryTag}image${i}`;
            gitlabYml += `
    - docker build --pull -t "$CI_REGISTRY${link}" ${image.folder}
    - docker push "$CI_REGISTRY${link}"`;
        });
        gitlabYml += "\n  only:\n    - main\n\n";
    }

    gitlabYml += `push-to-challenge-service:
  image: ubuntu:latest
  stage: build
  before_script:
    - apt-get update -y
    - apt-get install -y wget 
    - apt-get install -y zip 
    - wget https://github.com/mrtrkmnhub/cservice-cli/releases/download/1.0.0/cservice-cli_1.0.0_linux_64-bit.zip 
    - unzip cservice-cli_1.0.0_linux_64-bit.zip 
    - mv cservice-cli_1.0.0_linux_64-bit/main .
    - chmod +x ./main
  script: 
    - ./main
  only: 
    - main
  variables:
    ENDPOINT: "$ENDPOINT"
    PORT: "$PORT"
    AUTH_KEY: "$AUTH_KEY"
    SIGN_KEY: "$SIGN_KEY"\n`;

    $("#output-gitlab-yml").val(gitlabYml);
}

var update = function () {
    getImages();
    updateReadme();
    updateConfigYml();
    updateGitlabYml();
}

var loadTemplate = async function (template, node, replace) {
    await $.get(template, (data) => {
        node.append(data.replaceAll("XXX", replace));
        rebind();
    })
}

$("#addStaticImage").click(async function () {
    imageID += 1;
    await loadTemplate('templates/static_flag.html', $("#images"), imageID);
    if (imageID === 1) {
        $("#staticflagname1").val($("#challengename").val());
        $("#removeflagbutton1").remove();
    }
})

$("#addImage").click(async function () {
    imageID += 1;
    await loadTemplate('templates/image.html', $("#images"), imageID);
    if (imageID === 1) {
        $("#removeimagebutton1").remove();
    }

    // Always add first flag and disallow removing it
    flagID += 1;
    await loadTemplate('templates/dynamic_flag.html', $(`#addflagbutton${imageID}`).parent("div"), flagID);
    $(`#removeflagbutton${flagID}`).remove();
})

// Rebind on-click handlers to any added records (images, flags, etc.)
var rebind = function () {
    $(".removeImage").off().click(function () {
        $(this).parent("div").remove();
        update();
    });

    $(".addDNS").off().click(function () {
        dnsID += 1;
        loadTemplate('templates/dns.html', $(this).parent("div"), dnsID);
    })

    $(".removeDNS").off().click(function () {
        $(this).parent("div").remove();
        update();
    });

    $(".addEnv").off().click(function () {
        envID += 1;
        loadTemplate('templates/env.html', $(this).parent("div"), envID);
    })

    $(".removeEnv").off().click(function () {
        $(this).parent("div").remove();
        update();
    });

    $(".addFlag").off().click(function () {
        flagID += 1;
        loadTemplate('templates/dynamic_flag.html', $(this).parent("div"), flagID);
    })

    $(".removeFlag").off().click(function () {
        $(this).parent("div").remove();
        update();
    });

    update();
}

$(".limited").on("input", function () {
    var c = this.selectionStart;
    var r = /[^a-z0-9]/g;
    var v = $(this).val();
    if (r.test(v)) {
        $(this).val(v.replace(r, ""));
        c--;
    }
    this.setSelectionRange(c, c);
    update();
});

var downloadProject = async function () {
    // Extract project files from output text areas
    const files = [
        { name: "challenge/challenge-config/challenge.yml", input: $("#output-challenge-yml").val() },
        { name: "challenge/.gitlab-ci.yml", input: $("#output-gitlab-yml").val() },
        { name: "challenge/README.md", input: $("#output-readme").val() },
        { name: "challenge/src/" },
        { name: "challenge/solution/" },
    ]

    // Add empty dockerfiles for dynamic projects
    var isDynamic = $("#entry-type").is(":checked");
    if (isDynamic) {
        getImages().forEach(image => files.push(
            { name: `challenge/${image.folder}/Dockerfile`.replaceAll("/./", "/"), input: "" }
        ));
    }

    // Generate ZIP blob
    const blob = await downloadZip(files).blob()

    // Make and click temporary download link
    const link = document.createElement("a")
    link.href = URL.createObjectURL(blob)
    link.download = "haaukins-challenge.zip"
    link.click()
    link.remove()
}
